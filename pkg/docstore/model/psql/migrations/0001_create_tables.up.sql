CREATE TABLE "document" (
  "id" uuid PRIMARY KEY,
  "name" varchar NOT NULL,
  "location" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "mime_type_id" int,
  "file_size_id" int
);

CREATE TABLE "conversion" (
  "original_id" uuid,
  "document_id" uuid,
  PRIMARY KEY ("original_id", "document_id")
);

CREATE TABLE "mime_type" (
  "id" int PRIMARY KEY,
  "name" varchar NOT NULL,
  "tag" varchar UNIQUE
);

CREATE TABLE "file_size" (
  "id" int PRIMARY KEY,
  "name" varchar NOT NULL,
  "tag" varchar UNIQUE
);

ALTER TABLE "document" ADD FOREIGN KEY ("mime_type_id") REFERENCES "mime_type" ("id");

ALTER TABLE "document" ADD FOREIGN KEY ("file_size_id") REFERENCES "file_size" ("id");

ALTER TABLE "conversion" ADD FOREIGN KEY ("original_id") REFERENCES "document" ("id");

ALTER TABLE "conversion" ADD FOREIGN KEY ("document_id") REFERENCES "document" ("id");