package psql

import (
	"database/sql"

	"apyrolabs.com/sefleek/pkg/docstore/core/documents"
	"github.com/google/uuid"
	_ "github.com/lib/pq" //register driver
)

//DocumentRepository contains all the queries
//used agains the main Document database
type DocumentRepository struct {
	db *sql.DB //database pool
}

//DocumentRepo returns a psql implementation of the DocumentsRepo interface
func DocumentRepo(db *sql.DB) *DocumentRepository {
	return &DocumentRepository{db}
}

// GetDocumentByID returns the document for a given ID
func (repo *DocumentRepository) GetDocumentByID(id uuid.UUID) (*documents.Document, error) {
	stmt := `SELECT id, "name", "location", created_at
	FROM public.documents
	WHERE id=$1`

	doc := &documents.Document{}
	err := repo.db.QueryRow(stmt, id).Scan(&doc.ID, &doc.Name, &doc.Location, &doc.CreatedAt)
	if err != nil {
		return nil, err
	}

	return doc, nil
}

// CreateDocument stores a new document in the database
func (repo *DocumentRepository) CreateDocument(doc *documents.Document) (uuid.UUID, error) {
	return uuid.Nil, nil
}
