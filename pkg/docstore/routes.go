package docstore

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"runtime/debug"
	"strconv"

	"github.com/google/uuid"
)

// The serverERror helper writes an error message and stack trace to the errorLog
/// then sends a generic 500 internal Server Error response to the user
func (s *storage) serverError(w http.ResponseWriter, err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	s.errorLog.Output(2, trace)

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

// The clientError helper sends a speicif status code and corresponding descirption
// to the user when there is a problem with their request.
func (s *storage) clientError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
}

// This is a convenience wrapper around clientError which sends a 404 response to the user
func (s *storage) notFound(w http.ResponseWriter) {
	s.clientError(w, http.StatusNotFound)
}

func (s *storage) routes() *http.ServeMux {
	//create file server
	mux := http.NewServeMux()
	mux.HandleFunc("/static", s.filehandler)
	return mux
}

func (s *storage) filehandler(w http.ResponseWriter, r *http.Request) {
	idParam := r.URL.Query().Get("id")
	id, err := uuid.Parse(idParam)
	if err != nil {
		s.serverError(w, err)
		return
	}
	doc, err := s.docs.GetDocumentByID(id)
	if err != nil {
		s.serverError(w, err)
		return
	}
	f, err := os.Open(doc.Location)
	defer f.Close()
	if err != nil {
		s.serverError(w, err)
		return
	}

	//File is found, create and send the correct headers

	//Get the Content-Type of the file
	//Create a buffer to store the header of the file in
	FileHeader := make([]byte, 512)
	//Copy the headers into the FileHeader buffer
	f.Read(FileHeader)
	//Get content type of file
	FileContentType := http.DetectContentType(FileHeader)

	//Get the file size
	FileStat, _ := f.Stat()                            //Get info from file
	FileSize := strconv.FormatInt(FileStat.Size(), 10) //Get file size as a string

	//Send the headers
	w.Header().Set("Content-Disposition", "inline; filename="+doc.Name)
	w.Header().Set("Content-Type", FileContentType)
	w.Header().Set("Content-Length", FileSize)

	//Send the file
	//We read 512 bytes from the file already, so we reset the offset back to 0
	f.Seek(0, 0)
	io.Copy(w, f) //'Copy' the file to the client
	return

	////----------
	// reader := bufio.NewReader(f)
	// _, err = reader.WriteTo(w)
	// if err != nil {
	// 	s.serverError(w, err)
	// 	return
	// }

}
