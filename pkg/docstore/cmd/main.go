package main

import "apyrolabs.com/sefleek/pkg/docstore"

func main() {
	docCfg := &docstore.Config{
		Dsn:       "user=postgres password=password host=localhost port=5432 dbname=selfleek_documents sslmode=disable",
		Host:      ":4500",
		ServePath: "/",
		StaticDir: "",
	}
	docstore.StartServer(docCfg)
}
