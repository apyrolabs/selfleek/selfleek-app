package docstore

import (
	"database/sql"
	"log"

	"apyrolabs.com/sefleek/pkg/docstore/core/documents"
)

//Config is the configuration for a document store
type Config struct {
	Dsn       string
	Host      string
	StaticDir string
	ServePath string
}

type storage struct {
	cfg      *Config
	db       *sql.DB
	docs     *documents.Service
	errorLog *log.Logger
	infoLog  *log.Logger
}
