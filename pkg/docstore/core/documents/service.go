package documents

import (
	"fmt"

	"github.com/google/uuid"
)

// Service contains the function for
// interacting with document store
type Service struct {
	repo Repository
}

//New returns a new Document Service
func New(repo Repository) *Service {
	return &Service{repo}
}

// GetDocumentByID returns the document for a given ID
func (s *Service) GetDocumentByID(id uuid.UUID) (*Document, error) {
	return s.repo.GetDocumentByID(id)
}

// GetDocumentTemporaryURL returns a temporary url for a given document id
func (s *Service) GetDocumentTemporaryURL(id uuid.UUID) (string, error) {
	doc, err := s.GetDocumentByID(id)
	if err != nil {
		return "", err
	}

	url := fmt.Sprintf("http://localhost:4500/static?id=%s", doc.ID)
	return url, nil
}

// CreateDocument stores a new document in the database
func (s *Service) CreateDocument() (uuid.UUID, error) {
	return uuid.Nil, nil
}
