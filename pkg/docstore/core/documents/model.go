package documents

import (
	"time"

	"github.com/google/uuid"
)

// Document represents the documents table
type Document struct {
	ID        uuid.UUID
	Name      string
	Location  string
	CreatedAt time.Time
}
