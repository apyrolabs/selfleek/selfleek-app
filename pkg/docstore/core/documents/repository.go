package documents

import "github.com/google/uuid"

// Repository is an interface for interacting with
// document objects in a database
type Repository interface {
	// GetDocumentByID returns the document for a given ID
	GetDocumentByID(id uuid.UUID) (*Document, error)
	// CreateDocument stores a new document in the database
	CreateDocument(doc *Document) (uuid.UUID, error)
}
