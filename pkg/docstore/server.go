package docstore

import (
	"database/sql"
	"log"
	"net/http"
	"os"

	"apyrolabs.com/sefleek/pkg/docstore/core/documents"
	"apyrolabs.com/sefleek/pkg/docstore/model/psql"
)

//StartServer starts the file server
//should run as a go routine
func StartServer(cfg *Config) {

	// Create custom loggers.
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "Error\t", log.Ldate|log.Ltime|log.Lshortfile)

	//Open a database connection pool
	db, err := openDb(cfg.Dsn)
	if err != nil {
		errorLog.Fatal(err)
	}

	//Also, defer db.close() so that db closed before main exits
	defer db.Close()

	//repo
	repo := psql.DocumentRepo(db)
	// documents service
	docs := documents.New(repo)
	//storage
	strg := &storage{
		cfg:  cfg,
		db:   db,
		docs: docs,
	}

	// Create HTTP server
	srv := &http.Server{
		Addr:     cfg.Host,
		ErrorLog: errorLog,
		Handler:  strg.routes(),
	}

	infoLog.Printf("Serving Documents at: %s\n", cfg.Host)
	err = srv.ListenAndServe()
	errorLog.Fatal(err)
}

// Helper function for establishing DB pool
func openDb(dsn string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
