package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/google/uuid"
)

func (c *Controller) getSelfie(w http.ResponseWriter, r *http.Request) {
	s := r.URL.Query().Get("id")
	id, err := uuid.Parse(s)
	if err != nil {
		c.serverError(w, err)
		return
	}
	selfie, err := c.App.Selfies.Get(id)
	if err != nil {
		c.serverError(w, err)
		return
	}

	js, err := json.Marshal(selfie)
	if err != nil {
		c.serverError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write(js)
}

func (c *Controller) latestSelfies(w http.ResponseWriter, r *http.Request) {
	count, err := strconv.Atoi(r.URL.Query().Get("count"))
	if err != nil {
		c.serverError(w, err)
		return
	}
	selfies, err := c.App.Selfies.Latest(count)
	if err != nil {
		c.serverError(w, err)
		return
	}

	js, err := json.Marshal(selfies)
	if err != nil {
		c.serverError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write(js)
}

func (c *Controller) createSelfie(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Unimplemented")
}
