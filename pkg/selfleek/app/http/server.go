package http

import (
	"database/sql"
	"log"
	"net/http"
	"os"

	"apyrolabs.com/sefleek/pkg/selfleek/app"
)

//ServerConfig holds settings for
//host the http version of this application
type ServerConfig struct {
	Host string
	App  app.AppConfig
}

//StartServer initializes the app and runs the http server
func StartServer(cfg *ServerConfig) {

	// Create custom loggers.
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "Error\t", log.Ldate|log.Ltime|log.Lshortfile)

	//Open a database connection pool
	appDB, err := openDb(cfg.App.AppDsn)
	if err != nil {
		errorLog.Fatal(err)
	}
	defer appDB.Close() //Also, defer db.close() so that db closed before main exits

	docDB, err := openDb(cfg.App.DocumentDsn)
	if err != nil {
		errorLog.Fatal(err)
	}
	defer docDB.Close() //Also, defer db.close() so that db closed before main exits

	app := app.New(appDB, docDB, errorLog, infoLog)
	ctrlr := &Controller{
		App:      app,
		errorLog: errorLog,
		infoLog:  infoLog,
	}

	server := http.Server{
		Addr:     cfg.Host,
		ErrorLog: errorLog,
		Handler:  ctrlr.routes(),
	}

	infoLog.Printf("Serving application at: %s\n", cfg.Host)
	err = server.ListenAndServe()
	errorLog.Fatal(err)
}

// Helper function for establishing DB pool
func openDb(dsn string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
