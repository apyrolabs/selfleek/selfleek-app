package http

import (
	"log"
	"net/http"

	"apyrolabs.com/sefleek/pkg/selfleek/app"
)

type Controller struct {
	App      *app.Application
	errorLog *log.Logger
	infoLog  *log.Logger
}

func (c *Controller) routes() *http.ServeMux {
	// Use the http.NewServeMux() function to initialize a new servemux
	mux := http.NewServeMux()
	mux.HandleFunc("/selfies/get", c.getSelfie)
	mux.HandleFunc("/selfies/latest", c.latestSelfies)
	mux.HandleFunc("/selfies/create", c.createSelfie)

	return mux
}
