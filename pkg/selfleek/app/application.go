package app

import (
	"database/sql"
	"log"

	"apyrolabs.com/sefleek/pkg/docstore/core/documents"

	docpsql "apyrolabs.com/sefleek/pkg/docstore/model/psql"
	"apyrolabs.com/sefleek/pkg/selfleek/core/selfie"
	"apyrolabs.com/sefleek/pkg/selfleek/model/psql"
)

type Application struct {
	Selfies   *selfie.Service
	Documents *documents.Service
}

type AppConfig struct {
	AppDsn      string
	DocumentDsn string
}

//New retunrs a new instance of the application
func New(appDB, docDB *sql.DB, errorLog, infoLog *log.Logger) *Application {
	appRepo := psql.ApplicationRepo(appDB)
	docService := documents.New(docpsql.DocumentRepo(docDB))
	return &Application{
		Selfies: selfie.New(appRepo, errorLog, infoLog, docService),
	}
}
