CREATE TABLE "selfie" (
  "id" uuid PRIMARY KEY,
  "profile_id" uuid NOT NULL,
  "image_id" uuid NOT NULL,
  "caption" varchar(50),
  "created_at" timestamp NOT NULL,
  "published_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "profile" (
  "id" uuid PRIMARY KEY,
  "account_id" uuid,
  "username" varchar(20) UNIQUE NOT NULL,
  "name" varchar(30) NOT NULL,
  "website" varchar(50),
  "bio" varchar(100),
  "age" int,
  "photo_id" uuid
);

CREATE TABLE "follow" (
  "profile_id" uuid,
  "follows_id" uuid,
  "approved" timestamp,
  PRIMARY KEY ("profile_id", "follows_id")
);



ALTER TABLE "selfie" ADD FOREIGN KEY ("profile_id") REFERENCES "profile" ("id");

ALTER TABLE "follow" ADD FOREIGN KEY ("profile_id") REFERENCES "profile" ("id");

ALTER TABLE "follow" ADD FOREIGN KEY ("follows_id") REFERENCES "profile" ("id");
