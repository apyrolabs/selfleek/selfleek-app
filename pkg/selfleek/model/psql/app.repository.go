package psql

import (
	"database/sql"

	"apyrolabs.com/sefleek/pkg/selfleek/core/selfie"
	"github.com/google/uuid"
	_ "github.com/lib/pq" //register driver
)

//ApplicationRepository contains all the queries
//used agains the main application database
type ApplicationRepository struct {
	db *sql.DB //database pool
}

//ApplicationRepo returns a psql implementation of the SelfiesRepo interface
func ApplicationRepo(db *sql.DB) *ApplicationRepository {
	return &ApplicationRepository{db}
}

//Get returns the selfie with the given id
func (repo *ApplicationRepository) Get(id uuid.UUID) (*selfie.Selfie, error) {
	stmt := `SELECT id, "imageId", caption, created_at
	FROM public.selfies
	WHERE selfies.id=$1`

	s := &selfie.Selfie{} //selfie
	err := repo.db.QueryRow(stmt, id).Scan(&s.ID, &s.ImageID, &s.Caption, &s.CreatedAt)
	if err != nil {
		return nil, err
	}

	return s, err

}

//Latest returns the latest n selfies
func (repo *ApplicationRepository) Latest(n int) ([]*selfie.Selfie, error) {
	stmt := `SELECT id, "imageId", caption, created_at
	FROM public.selfies
	ORDER BY created_at DESC
	FETCH FIRST $1 ROWS ONLY`

	rows, err := repo.db.Query(stmt, n)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	selfies := make([]*selfie.Selfie, 0, n) //empty selfie slice with space for n selfies
	for rows.Next() {
		s := &selfie.Selfie{}
		err := rows.Scan(&s.ID, &s.ImageID, &s.Caption, &s.CreatedAt)
		if err != nil {
			return nil, err
		}
		selfies = append(selfies, s)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return selfies, nil
}

//Create makes a new selfie and returns the given Id.
func (repo *ApplicationRepository) Create(imageID uuid.UUID, caption string) (*uuid.UUID, error) {
	return nil, nil
}
