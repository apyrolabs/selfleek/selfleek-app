package selfie

import (
	"log"

	"apyrolabs.com/sefleek/pkg/docstore/core/documents"

	"github.com/google/uuid"
)

//Service contains the business logic
//for selfie management
type Service struct {
	selfies  Repository
	errorLog *log.Logger
	infoLog  *log.Logger
	docs     *documents.Service
}

//New returns a new instance of a Selfie Serivce
func New(selfies Repository, errorLog, infoLog *log.Logger, docs *documents.Service) *Service {
	return &Service{selfies, errorLog, infoLog, docs}
}

//Get returns the selfie with the given id
func (srv *Service) Get(id uuid.UUID) (*SelfiePreview, error) {
	selfie, err := srv.selfies.Get(id)
	if err != nil {
		return nil, err
	}
	imgURL, err := srv.docs.GetDocumentTemporaryURL(selfie.ImageID)
	if err != nil {
		return nil, err
	}
	return &SelfiePreview{
		ID:        selfie.ID,
		Caption:   selfie.Caption,
		CreatedAt: selfie.CreatedAt,
		ImageURL:  imgURL,
	}, nil
}

//Latest returns the latest n selfies
func (srv *Service) Latest(n int) ([]*SelfiePreview, error) {
	selfies, err := srv.selfies.Latest(n)
	if err != nil {
		return nil, err
	}

	selfiesMapped := make([]*SelfiePreview, 0, n)
	for _, selfie := range selfies {
		imgURL, err := srv.docs.GetDocumentTemporaryURL(selfie.ImageID)
		if err != nil {
			return nil, err
		}
		preview := &SelfiePreview{
			ID:        selfie.ID,
			Caption:   selfie.Caption,
			CreatedAt: selfie.CreatedAt,
			ImageURL:  imgURL,
		}
		selfiesMapped = append(selfiesMapped, preview)
	}

	return selfiesMapped, nil
}

//Create makes a new selfie and returns the given Id.
func (srv *Service) Create(imageID uuid.UUID, caption string) (*uuid.UUID, error) {
	return srv.selfies.Create(imageID, caption)
}
