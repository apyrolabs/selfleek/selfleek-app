package selfie

import "github.com/google/uuid"

//Repository provides methods for
//interacting with selfie data
type Repository interface {
	//Get returns the selfie with the given id
	Get(id uuid.UUID) (*Selfie, error)
	//Latest returns the latest n selfies
	Latest(n int) ([]*Selfie, error)
	//Create makes a new selfie and returns the given Id.
	Create(imageID uuid.UUID, caption string) (*uuid.UUID, error)
}
