package selfie

import (
	"time"

	"github.com/google/uuid"
)

type SelfiePreview struct {
	ID        uuid.UUID
	Caption   string
	CreatedAt time.Time
	ImageURL  string
}
