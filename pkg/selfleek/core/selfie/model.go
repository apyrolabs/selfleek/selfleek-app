package selfie

import (
	"time"

	"github.com/google/uuid"
)

//Selfie represents a user posted image
type Selfie struct {
	ID        uuid.UUID
	ImageID   uuid.UUID
	Caption   string
	CreatedAt time.Time
}
