package incredigo

import "log"

type mockEmailService struct {
	infoLog *log.Logger
}

//NewMockEmailService returns a new email service that prints emails
//to the debug console
func NewMockEmailService(infoLog *log.Logger) *mockEmailService {
	return &mockEmailService{
		infoLog: infoLog,
	}
}

func (s *mockEmailService) SendEmail(email, message string) error {
	s.infoLog.Printf(`Sending email: {
		Email: %v,
		Message: %v
	}`, email, message)

	return nil
}
