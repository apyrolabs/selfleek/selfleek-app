package auth

//Config holds settings related to auth.
type Config struct {
	PhoneVerificationMessagePreFix   string
	PhoneVerificationLifeSpanMinutes int
	EmailVerificationMessagePreFix   string
	EmailVerificationLifeSpanMinutes int
}

//PhoneMessageService provides messaging to cellular devices
type PhoneMessageService interface {
	SendMessage(phone, message string) error
}

//EmailService provides emailing to users
type EmailService interface {
	SendEmail(email, message string) error
}
