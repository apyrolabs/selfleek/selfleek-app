package auth

import (
	"errors"
)

func checkPassword(password string) error {
	if !meetsLengthRequirment(password) {
		return errors.New("password does not meet minimum length requirement")
	}

	return nil
}

func meetsLengthRequirment(password string) bool {
	return len(password) > 7
}
