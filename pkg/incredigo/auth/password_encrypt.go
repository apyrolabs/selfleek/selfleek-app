package auth

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"crypto/subtle"
	"fmt"

	"golang.org/x/crypto/scrypt"
)

const (
	// scrypt is used for strong keys
	// these are the recommended scrypt parameters
	scryptN      = 16384
	scryptR      = 8
	scryptP      = 1
	scryptKeyLen = 32
)

type PEncrypt struct {
}

type Token struct {
	Username string `json:"user"`
	Password string `json:"pwd"`
	Salt     string `json:"salt"`
}

func (p PEncrypt) randbytes(count int) ([]byte, error) {
	// Generate a salt
	salt := make([]byte, count)
	_, err := rand.Read(salt)
	return salt, err
}

func (p PEncrypt) hmacSha256(in, salt []byte) ([]byte, error) {
	mac := hmac.New(sha256.New, salt)
	_, err := mac.Write(in)
	if err != nil {
		return nil, err
	}
	return mac.Sum(nil), nil
}

func (p PEncrypt) encScrypt(in, salt []byte) ([]byte, error) {
	return scrypt.Key(in, salt, scryptN, scryptR, scryptP, scryptKeyLen)
}

//HashPassword returns a hashed password
func (p PEncrypt) HashPassword(password, salt, pepper []byte) ([]byte, error) {

	peppered, _ := p.hmacSha256(password, pepper)
	cur, _ := p.encScrypt(peppered, salt)

	return cur, nil
}

//Authenticate authenticates a password
func (p PEncrypt) Authenticate(password, salt, pepper, hash []byte) (bool, error) {
	h, _ := p.HashPassword(password, salt, pepper)

	if subtle.ConstantTimeCompare(h, hash) != 1 {
		return false, fmt.Errorf("Invalid username or password")
	}

	return true, nil
}

// func main() {
// 	salt, _ := randbytes(16)
// 	cur, _ := HashPassword(Password, salt)

// 	t := Token{
// 		Username: string(User),
// 		Password: base64.StdEncoding.EncodeToString(cur),
// 		Salt:     base64.StdEncoding.EncodeToString(salt),
// 	}

// 	if ok, _ := Authenticate(Password, salt, cur); ok {
// 		fmt.Println("OK")
// 	}

// 	payload, _ := json.Marshal(t)
// 	fmt.Println(string(payload))
// }
