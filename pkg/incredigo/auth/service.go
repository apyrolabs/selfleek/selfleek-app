package auth

import (
	"errors"
	"fmt"
	"log"
	"strconv"

	"github.com/google/uuid"
	errs "github.com/pkg/errors"
)

//Service provides functions for managing
//account data.
type Service struct {
	repo         Repository
	phoneService PhoneMessageService
	emailService EmailService
	config       *Config
	infoLog *log.Logger
	errorLog *log.Logger
}

var encrypt = PEncrypt{}
var pepper = []byte("ThisIsATemporaryPepperForEncryption")

//NewService returns a new authenication.Service
func NewService(conf *Config, repo Repository, phoneService PhoneMessageService, emailService EmailService, infoLog, errorLog *log.Logger) *Service {
	return &Service{
		repo:         repo,
		phoneService: phoneService,
		config:       conf,
		emailService: emailService,
		infoLog: infoLog,
		errorLog: errorLog,
	}
}

//Register creates a new user from the given input
func (s *Service) Register(registration *RegistrationInput) (*uuid.UUID, error) {

	if err := checkPassword(*registration.Password); err != nil {
		return nil, err
	}

	salt, err := encrypt.randbytes(16)
	if err != nil {
		return nil, err
	}
	hashedPassword, err := encrypt.HashPassword([]byte(*registration.Password), salt, pepper)
	if err != nil {
		return nil, err
	}

	id, err := s.repo.CreateAccount(registration.Username,
		registration.Email, registration.Phone, hashedPassword, salt)

	return id, err
}

//AuthByUser authorizes by username
func (s *Service) AuthByUser(login *UserAuthInput) (bool, error) {

	account, err := s.repo.GetAccountByUsername(login.Username)
	if err != nil {
		return false, err
	}

	pass, err := s.repo.GetLatestAccountPassword(&account.ID)
	if err != nil {
		return false, err
	}

	_, err = encrypt.Authenticate([]byte(*login.Password), pass.Salt, pepper, pass.Password)
	if err != nil {
		return false, err
	}

	return true, nil
}

//AuthByEmail authorizes by Email
func (s *Service) AuthByEmail(login *EmailAuthInput) (bool, error) {

	account, err := s.repo.GetAccountByEmail(login.Email)
	if err != nil {
		return false, err
	}

	pass, err := s.repo.GetLatestAccountPassword(&account.ID)
	if err != nil {
		return false, err
	}

	_, err = encrypt.Authenticate([]byte(*login.Password), pass.Salt, pepper, pass.Password)
	if err != nil {
		return false, err
	}

	return true, nil
}

//AuthByPhone authorizes by phone number
func (s *Service) AuthByPhone(login *PhoneAuthInput) (bool, error) {

	account, err := s.repo.GetAccountByPhone(login.Phone)
	if err != nil {
		return false, err
	}

	pass, err := s.repo.GetLatestAccountPassword(&account.ID)
	if err != nil {
		return false, err
	}

	_, err = encrypt.Authenticate([]byte(*login.Password), pass.Salt, pepper, pass.Password)
	if err != nil {
		return false, err
	}

	return true, nil
}

//RequestVerification issues a verification request
func (s *Service) createVerification(accountID uuid.UUID, verificationTypeID, durationMinutes int, key string) (*uuid.UUID, error) {

	salt, err := encrypt.randbytes(16)
	if err != nil {
		return nil, err
	}

	cryptKey, err := encrypt.HashPassword([]byte(key), salt, pepper)
	if err != nil {
		return nil, err
	}

	return s.repo.CreateVerification(accountID, cryptKey, salt, verificationTypeID, durationMinutes)
}

//RequestPhoneVerification sends a message to the users phone with the verification key.
func (s *Service) RequestPhoneVerification(accountID uuid.UUID) error {

	//get the verification type
	vtype, err := s.repo.GetVerificationTypeBySlug("phone_verification")
	if err != nil {
		return err
	}

	key := strconv.Itoa(random7Digits()) // create a random 7 digit key
	//Create the verification record
	_, err = s.createVerification(accountID, vtype.ID, s.config.PhoneVerificationLifeSpanMinutes, key)
	if err != nil {
		return err
	}

	//Get the users phone number
	phone, err := s.repo.GetPhoneByAccountID(&accountID)
	if err != nil {
		return err
	}

	//send the key to the users phone
	err = s.sendTextMessage(phone.Phone,
		fmt.Sprintf("%v: Your verification code is %v",
			s.config.PhoneVerificationMessagePreFix, key))

	if err != nil {
		return errs.Wrap(err, "failed to send message")
	}
	return nil
}

func (s *Service) sendTextMessage(phone, message string) error {
	return s.phoneService.SendMessage(phone, message)
}

//RequestEmailVerification sends a message to the users email with the verification key.
func (s *Service) RequestEmailVerification(accountID uuid.UUID) error {

	//get the verification type
	vtype, err := s.repo.GetVerificationTypeBySlug("email_verification")
	if err != nil {
		return err
	}

	key := strconv.Itoa(random7Digits()) // create a random 7 digit key
	//Create the verification record
	_, err = s.createVerification(accountID, vtype.ID, s.config.EmailVerificationLifeSpanMinutes, key)
	if err != nil {
		return err
	}

	//Get the users email
	email, err := s.repo.GetEmailByAccountID(&accountID)
	if err != nil {
		return err
	}

	//send the key to the users email
	err = s.sendEmail(email.Email,
		fmt.Sprintf("%v: Your verification code is %v",
			s.config.EmailVerificationMessagePreFix, key))

	if err != nil {
		return errs.Wrap(err, "failed to send email")
	}
	return nil
}

func (s *Service) sendEmail(email, message string) error {
	return s.emailService.SendEmail(email, message)
}

//VerifyPhone sets the phone verified date for a given user
//if the key is correct and has not expired
func (s *Service) VerifyPhone(accountID uuid.UUID, key string) error {
	//get the verification type
	vtype, err := s.repo.GetVerificationTypeBySlug("phone_verification")
	if err != nil {
		return err
	}
	v, err := s.repo.GetMostRecentVerification(accountID, vtype.ID)
	if err != nil {
		return err
	}

	//Get current database time
	now, err := s.repo.Now()
	if err != nil {
		return errs.Wrap(err, "unable to get time from database")
	}

	//compare now with verifications expiration
	s.infoLog.Printf(`Comparing times: 
		Now: %s
		Exipration: %s`, now, v.ExpiresAt)
	expiration := v.ExpiresAt
	if now.After(expiration) {
		return errors.New("the verification key has already expired")
	}

	//Test the key
	_, err = encrypt.Authenticate([]byte(key), v.Salt, pepper, v.Key)
	if err != nil {
		return errs.Wrap(err, "an incorrect key was provided")
	}

	//Get users phone
	phone, err := s.repo.GetPhoneByAccountID(&accountID)
	if err != nil {
		return errs.Wrap(err, "can not find phone for this user")
	}

	//Mark the users phone as verified
	phone.VerifiedAt = &now

	//Update the phone record
	err = s.repo.UpdatePhone(phone)
	if err!= nil {
		return errs.Wrap(err, "trouble marking the phone as verified")
	}

	return nil
}

//VerifyEmail sets the email verified date for a given user
//if the key is correct and has not expired
func (s *Service) VerifyEmail(accountID uuid.UUID, key string) error {
	//get the verification type
	vtype, err := s.repo.GetVerificationTypeBySlug("email_verification")
	if err != nil {
		return err
	}
	v, err := s.repo.GetMostRecentVerification(accountID, vtype.ID)
	if err != nil {
		return err
	}

	//Get current database time
	now, err := s.repo.Now()
	if err != nil {
		return errs.Wrap(err, "unable to get time from database")
	}

	//compare now with verifications expiration
	if now.After(v.ExpiresAt) {
		return errors.New("the verification key has already expired")
	}

	//Test the key
	_, err = encrypt.Authenticate([]byte(key), v.Salt, pepper, v.Key)
	if err != nil {
		return errs.Wrap(err, "an incorrect key was provided")
	}

	//Get users email
	email, err := s.repo.GetEmailByAccountID(&accountID)
	if err != nil {
		return errs.Wrap(err, "can not find phone for this user")
	}

	//Mark the users phone as verified
	email.VerifiedAt = &now

	//Update the email record
	err = s.repo.UpdateEmail(email)
	if err!= nil {
		return errs.Wrap(err, "trouble marking the email as verified")
	}

	return nil
}