package auth

import (
	"math/rand"
	"time"
  )

  
var max7Digits = 0x96C089

func random7Digits() int {
	seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))
	return seededRand.Intn(max7Digits)
}