package auth

import (
	"time"

	"github.com/google/uuid"
)

//Repository provides methods to manipulate
//account data.
type Repository interface {
	CreateAccount(username, email, phone *string, password, salt []byte) (*uuid.UUID, error)
	GetLatestAccountPassword(accountID *uuid.UUID) (*Password, error)
	GetPhoneByAccountID(accountID *uuid.UUID) (*Phone, error)
	GetEmailByAccountID(accountID *uuid.UUID) (*Email, error)
	GetUsernameByAccountID(accountID *uuid.UUID) (*Username, error)
	GetAccountByUsername(username *string) (*Account, error)
	GetAccountByEmail(email *string) (*Account, error)
	GetAccountByPhone(phone *string) (*Account, error)
	CreateVerification(accoundID uuid.UUID, key, salt []byte, verificationTypeID, durationMinutes int) (*uuid.UUID, error)
	GetVerificationTypeBySlug(slug string) (*VerificationType, error)
	GetMostRecentVerification(accountID uuid.UUID, vTypeID int) (*Verification, error)
	Now() (time.Time, error)
	UpdatePhone(phone *Phone) error
	UpdateEmail(email *Email) error
}
