package auth

import (
	"time"

	"github.com/google/uuid"
)

//Account holds poperties of an account
type Account struct {
	ID        uuid.UUID
	CreatedAt time.Time
	DeletedAt *time.Time
}

//Password represents an account password
type Password struct {
	ID        uuid.UUID
	AccountID uuid.UUID
	Password  []byte
	Salt      []byte
	CreatedAt time.Time
	DeletedAt *time.Time
}

//Phone represents an account phone number
type Phone struct {
	AccountID  uuid.UUID
	Phone      string
	VerifiedAt *time.Time
	CreatedAt  time.Time
}

//Email represent and Account Email
type Email struct {
	AccountID  uuid.UUID
	Email      string
	VerifiedAt *time.Time
	CreatedAt  time.Time
}

//Username represents and account username
type Username struct {
	AccountID uuid.UUID
	Username  string
	CreatedAt time.Time
}

//Verification represents a request for verification
type Verification struct {
	ID                 uuid.UUID
	AccountID          uuid.UUID
	VerificationTypeID int
	Key                []byte
	Salt               []byte
	CreatedAt          time.Time
	ExpiresAt          time.Time
}

//VerificationType represents a type of Verification
type VerificationType struct {
	ID   int
	Name string
	Slug string
}

//RegistrationInput is used when creating a new account.
type RegistrationInput struct {
	Username *string
	Email    *string
	Phone    *string
	Password *string
}

type UserAuthInput struct {
	Username *string
	Password *string
}

type EmailAuthInput struct {
	Email    *string
	Password *string
}

type PhoneAuthInput struct {
	Phone    *string
	Password *string
}

type VerificationRequestInput struct {
	AccoundID        *string
	VerificationType *string
}
