package incredigo

import (
	"apyrolabs.com/sefleek/pkg/incredigo/auth"
	"apyrolabs.com/sefleek/pkg/incredigo/database/psql"
	"apyrolabs.com/sefleek/pkg/incredigo/http"
	"database/sql"
	"log"
	"os"
)

//CredentialService container for other services
type CredentialService struct {
	authentication *auth.Service
}

//NewService returns new instance of CredentialService
// func NewService(db *sql.DB) *CredentialService {
// 	auth := auth.NewService(&psql.AuthRepository{DB: db})
// 	return &CredentialService{auth}
// }

//Config holds the settings for this application
type Config struct {
	AuthDSN        string
	Authentication *auth.Config
	HTTP           *http.Config
	AuthDBEngine   string
}

type Server struct {
	config       *Config
	infoLog      *log.Logger
	errorLog     *log.Logger
	phoneService auth.PhoneMessageService
	emailService auth.EmailService
	authRepo     auth.Repository
	authService  *auth.Service
	authDB       *sql.DB
}

func NewServer(conf *Config) *Server {
	// Create custom loggers.
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "Error\t", log.Ldate|log.Ltime|log.Lshortfile)

	phoneService := NewMockPhoneService(infoLog)
	emailService := NewMockEmailService(infoLog)

	//Open a database connection pool
	authDB, err := openDb(conf.AuthDSN)
	if err != nil {
		errorLog.Fatal(err)
	}

	authRepo := &psql.AuthRepository{DB: authDB}
	authService := auth.NewService(
		conf.Authentication,
		authRepo,
		phoneService,
		emailService,
		infoLog,
		errorLog,
	)

	return &Server{
		config:       conf,
		infoLog:      infoLog,
		errorLog:     errorLog,
		phoneService: phoneService,
		emailService: emailService,
		authRepo:     authRepo,
		authService:  authService,
		authDB:       authDB,
	}

}

//Close cleans up connections
func (s *Server) Close() {
	s.authDB.Close() //close auth database
}

func (s *Server) HTTPServer() *http.Server {
	return http.NewServer(
			s.config.HTTP,
			s.infoLog,
			s.errorLog,
			s.authService,
		)
}

// Helper function for establishing DB pool
func openDb(dsn string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
