package psql

import (
	"database/sql"
	"errors"
	"time"

	"apyrolabs.com/sefleek/pkg/incredigo/auth"
	"github.com/google/uuid"
	_ "github.com/lib/pq" //register driver
	errs "github.com/pkg/errors"
)

//AuthRepository provides methods for accessing
// the user database
type AuthRepository struct {
	DB *sql.DB
}

// CreateAccount adds a new account to the database and optionally creates and relates
// email, phone, and password/salt records to that account if they are not nil
func (r *AuthRepository) CreateAccount(username, email, phone *string, password, salt []byte) (*uuid.UUID, error) {
	tx, err := r.DB.Begin()
	if err != nil {
		return nil, err
	}
	now, err := r.Now()
	if err != nil {
		return nil, err
	}
	accountID := uuid.New() //The ID for the new account

	//Insert new account record
	//with a newly generated UUID
	accountStmt := `INSERT INTO account
	(id, created_at)
	VALUES($1, $2);`

	_, err = tx.Exec(accountStmt, accountID, now)
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	//Insert new account_password record
	//with a newly generated UUID and the accountID
	if password != nil {
		passwordStmt := `INSERT INTO account_password
		(id, account_id, password, salt, created_at)
		VALUES($1, $2, $3, $4, $5);`
		passwordID := uuid.New()
		_, err = tx.Exec(passwordStmt, passwordID, accountID, password, salt, now)
		if err != nil {
			tx.Rollback()
			return nil, err
		}
	}

	// If a username was provided, create a new account_username
	// record for the generate Account UUID.
	if username != nil {
		stmt := `INSERT INTO account_username
		(account_id, username, created_at)
		VALUES($1, $2, $3);`

		_, err = tx.Exec(stmt, accountID, username, now)
		if err != nil {
			tx.Rollback()
			return nil, err
		}
	}

	// If an email was provided, create a new account_email
	// record for the generate Account UUID.
	if email != nil {
		stmt := `INSERT INTO account_email
		(account_id, email, created_at)
		VALUES($1, $2, $3);`

		_, err = tx.Exec(stmt, accountID, email, now)
		if err != nil {
			tx.Rollback()
			return nil, err
		}
	}

	// If a phone was provided, create a new account_phone
	// record for the generate Account UUID.
	if phone != nil {
		stmt := `INSERT INTO account_phone
		(account_id, phone, created_at)
		VALUES($1, $2, $3);`

		_, err = tx.Exec(stmt, accountID, phone, now)
		if err != nil {
			tx.Rollback()
			return nil, err
		}
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	return &accountID, nil
}

//GetLatestAccountPassword returns the most recent password for a given
func (r *AuthRepository) GetLatestAccountPassword(accountID *uuid.UUID) (*auth.Password, error) {
	if accountID == nil {
		return nil, errors.New("parameter accoundID *UUID must not be nil")
	}

	stmt := `SELECT id, account_id, password, salt, created_at, deleted_at
	FROM account_password
	WHERE account_id=$1
	ORDER BY created_at DESC
	LIMIT 1;`
	row := r.DB.QueryRow(stmt, accountID)

	p := &auth.Password{}

	err := row.Scan(&p.ID, &p.AccountID, &p.Password, &p.Salt, &p.CreatedAt, &p.DeletedAt)
	if err != nil {
		return nil, err
	}

	return p, nil
}

//GetPhoneByAccountID returns the account_phone record associated with the given accountID
func (r *AuthRepository) GetPhoneByAccountID(accountID *uuid.UUID) (*auth.Phone, error) {
	if accountID == nil {
		return nil, errors.New("parameter accoundID *UUID must not be nil")
	}

	stmt := `SELECT account_id, phone, verified_at, created_at
	FROM account_phone
	WHERE account_id=$1;`
	row := r.DB.QueryRow(stmt, accountID)

	p := &auth.Phone{}

	err := row.Scan(&p.AccountID, &p.Phone, &p.VerifiedAt, &p.CreatedAt)
	if err != nil {
		return nil, err
	}

	return p, nil
}

//GetEmailByAccountID returns the acount_email record with the assoicated accountID
func (r *AuthRepository) GetEmailByAccountID(accountID *uuid.UUID) (*auth.Email, error) {
	if accountID == nil {
		return nil, errors.New("parameter accoundID *UUID must not be nil")
	}

	stmt := `SELECT account_id, email, verified_at, created_at
	FROM account_email
	WHERE account_id=$1;`
	row := r.DB.QueryRow(stmt, accountID)

	e := &auth.Email{}

	err := row.Scan(&e.AccountID, &e.Email, &e.VerifiedAt, &e.CreatedAt)
	if err != nil {
		return nil, err
	}

	return e, nil
}

//GetUsernameByAccountID returns the account_username record with the assoicated accountID
func (r *AuthRepository) GetUsernameByAccountID(accountID *uuid.UUID) (*auth.Username, error) {
	if accountID == nil {
		return nil, errors.New("parameter accoundID *UUID must not be nil")
	}

	stmt := `SELECT account_id, username, created_at
	FROM account_username
	WHERE account_id=$1;`
	row := r.DB.QueryRow(stmt, accountID)

	u := &auth.Username{}

	err := row.Scan(&u.AccountID, &u.Username, &u.CreatedAt)
	if err != nil {
		return nil, err
	}

	return u, nil
}

//GetAccountByUsername returns the account record with the assoicated username
func (r *AuthRepository) GetAccountByUsername(username *string) (*auth.Account, error) {
	if username == nil {
		return nil, errors.New("parameter username *string must not be nil")
	}

	stmt := `SELECT account.id, account.created_at, account.deleted_at
	FROM account AS account JOIN account_username AS username ON (account.id = username.account_id)
	WHERE username.username=$1;`

	row := r.DB.QueryRow(stmt, username)

	a := &auth.Account{}

	err := row.Scan(&a.ID, &a.CreatedAt, &a.DeletedAt)
	if err != nil {
		return nil, err
	}

	return a, nil
}

//GetAccountByEmail returns the account record with the assoicated username
func (r *AuthRepository) GetAccountByEmail(email *string) (*auth.Account, error) {
	if email == nil {
		return nil, errors.New("parameter email *string must not be nil")
	}

	stmt := `SELECT account.id, account.created_at, account.deleted_at
	FROM account AS account JOIN account_email AS email ON (account.id = email.account_id)
	WHERE email.email=$1;`

	row := r.DB.QueryRow(stmt, email)

	a := &auth.Account{}

	err := row.Scan(&a.ID, &a.CreatedAt, &a.DeletedAt)
	if err != nil {
		return nil, err
	}

	return a, nil
}

//GetAccountByPhone returns the account record with the assoicated username
func (r *AuthRepository) GetAccountByPhone(phone *string) (*auth.Account, error) {
	if phone == nil {
		return nil, errors.New("parameter phone *string must not be nil")
	}

	stmt := `SELECT account.id, account.created_at, account.deleted_at
	FROM account AS account JOIN account_phone AS phone ON (account.id = phone.account_id)
	WHERE phone.phone=$1;`

	row := r.DB.QueryRow(stmt, phone)

	a := &auth.Account{}

	err := row.Scan(&a.ID, &a.CreatedAt, &a.DeletedAt)
	if err != nil {
		return nil, err
	}

	return a, nil
}

//GetVerificationTypeBySlug returns the VerificationType record with the given slug
func (r *AuthRepository) GetVerificationTypeBySlug(slug string) (*auth.VerificationType, error) {

	stmt := `SELECT id, name, slug
	FROM verification_type
	WHERE verification_type.slug=$1;`

	row := r.DB.QueryRow(stmt, slug)

	v := &auth.VerificationType{}

	err := row.Scan(&v.ID, &v.Name, &v.Slug)
	if err != nil {
		return nil, err
	}

	return v, nil
}

//CreateVerification enters a new Verification record
func (r *AuthRepository) CreateVerification(accoundID uuid.UUID, key, salt []byte, verificationTypeID, durationMinutes int) (*uuid.UUID, error) {
	tx, err := r.DB.Begin()
	if err != nil {
		return nil, err
	}

	stmt := `INSERT INTO verification
	(id, account_id, key, salt, verification_type_id, created_at, expires_at)
	VALUES($1, $2, $3, $4, $5, $6, $7);`

	id := uuid.New()
	now, err := r.Now()
	if err != nil {
		return nil, err
	}
	expire := now.Add(time.Minute * time.Duration(durationMinutes))

	//execute the insert
	_, err = tx.Exec(stmt, id, accoundID, key, salt, verificationTypeID, now, expire)
	if err != nil {
		tx.Rollback() //rollback on failure
		return nil, err
	}

	//Commit the transaction
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	return &id, nil
}

//Now returns the current time of the database.
func (r *AuthRepository) Now() (time.Time, error) {
	stmt := `SELECT NOW();`
	now := time.Time{}
	row := r.DB.QueryRow(stmt)
	err := row.Scan(&now)
	if err != nil {
		return now, err
	}
	return now, nil
}

func (r *AuthRepository) GetMostRecentVerification(accountID uuid.UUID, vTypeID int) (*auth.Verification, error) {
	stmt := `SELECT id, account_id, key, salt, verification_type_id, created_at, expires_at
	FROM verification
	WHERE account_id=$1 AND verification_type_id=$2
	ORDER BY expires_at DESC
	LIMIT 1;`

	v := auth.Verification{}
	row := r.DB.QueryRow(stmt, accountID, vTypeID)
	err := row.Scan(
		&v.ID, &v.AccountID, &v.Key, &v.Salt, &v.VerificationTypeID, &v.CreatedAt, &v.ExpiresAt,
	)

	if err != nil {
		return nil, errs.Wrap(err, "Query failed to get most recent verification")
	}

	return &v, nil
}

func (r *AuthRepository) UpdatePhone(phone *auth.Phone) error {
	stmt:=`UPDATE account_phone
	SET phone=$1, verified_at=$2, created_at=$3
	WHERE account_id=$4;`

	_, err := r.DB.Exec(stmt, phone.Phone, phone.VerifiedAt, phone.CreatedAt, phone.AccountID)
	if err!= nil {
		return errs.Wrap(err, "could not update phone record")
	}

	return nil
}

func (r *AuthRepository) UpdateEmail(email *auth.Email) error {
	stmt:=`UPDATE account_email
	SET email=$1, verified_at=$2, created_at=$3
	WHERE account_id=$4;`

	_, err := r.DB.Exec(stmt, email.Email, email.VerifiedAt, email.CreatedAt, email.AccountID)
	if err!= nil {
		return errs.Wrap(err, "could not update email record")
	}

	return nil
}



