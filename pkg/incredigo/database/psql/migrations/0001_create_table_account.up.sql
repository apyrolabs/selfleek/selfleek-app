CREATE TABLE "account" (
  "id" uuid PRIMARY KEY,
  "created_at" timestamp NOT NULL,
  "deleted_at" timestamp
);

CREATE TABLE "account_password" (
  "id" uuid PRIMARY KEY,
  "account_id" uuid NOT NULL,
  "password" bytea NOT NULL,
  "salt" bytea NOT NULL,
  "created_at" timestamp NOT NULL,
  "deleted_at" timestamp
);

CREATE TABLE "account_phone" (
  "account_id" uuid PRIMARY KEY,
  "phone" varchar(15) UNIQUE NOT NULL,
  "verified_at" timestamp,
  "created_at" timestamp NOT NULL
);

CREATE TABLE "account_email" (
  "account_id" uuid PRIMARY KEY,
  "email" varchar(50) UNIQUE NOT NULL,
  "verified_at" timestamp,
  "created_at" timestamp NOT NULL
);

CREATE TABLE "account_username" (
  "account_id" uuid PRIMARY KEY,
  "username" varchar(50) UNIQUE NOT NULL,
  "created_at" timestamp NOT NULL
);

CREATE TABLE "verification" (
  "id" uuid PRIMARY KEY,
  "account_id" uuid NOT NULL,
  "key" bytea NOT NULL,
  "salt" bytea NOT NULL,
  "verification_type_id" int NOT NULL,
  "created_at" timestamp NOT NULL,
  "expires_at" timestamp NOT NULL
);

CREATE TABLE "verification_type" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL,
  "slug" varchar UNIQUE NOT NULL
);


ALTER TABLE "account_password" ADD FOREIGN KEY ("account_id") REFERENCES "account" ("id");

ALTER TABLE "account_phone" ADD FOREIGN KEY ("account_id") REFERENCES "account" ("id");

ALTER TABLE "account_email" ADD FOREIGN KEY ("account_id") REFERENCES "account" ("id");

ALTER TABLE "account_username" ADD FOREIGN KEY ("account_id") REFERENCES "account" ("id");

ALTER TABLE "verification" ADD FOREIGN KEY ("account_id") REFERENCES "account" ("id");

ALTER TABLE "verification" ADD FOREIGN KEY ("verification_type_id") REFERENCES "verification_type" ("id");