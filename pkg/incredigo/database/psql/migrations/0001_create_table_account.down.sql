DROP TABLE IF EXISTS "account";
DROP TABLE IF EXISTS "account_password";
DROP TABLE IF EXISTS "account_phone";
DROP TABLE IF EXISTS "account_email";
DROP TABLE IF EXISTS "account_username";
DROP TABLE IF EXISTS "verification";
DROP TABLE IF EXISTS "verification_type";