package incredigo

import (
	"apyrolabs.com/sefleek/pkg/incredigo/auth"
	"apyrolabs.com/sefleek/pkg/incredigo/http"
)

//ServiceBuilder is Used to construct a new incredigo service
type ServerBuilder struct {
	Config *Config
}

var DefaultConfig = &Config{
	AuthDBEngine: "postgres",
	AuthDSN:      "user=postgres password=password host=localhost port=5432 dbname=auth sslmode=disable",
	Authentication: &auth.Config{
		PhoneVerificationMessagePreFix:   "Incredigo - Here is your verification code",
		PhoneVerificationLifeSpanMinutes: 20,
		EmailVerificationMessagePreFix:   "Incredigo - Here is your verification code",
		EmailVerificationLifeSpanMinutes: 20,
	},
	HTTP: &http.Config{Host: "localhost:4999"},
}

// Builder returns a ServiceBuilder
func Builder() *ServerBuilder {
	return &ServerBuilder{Config:DefaultConfig}
}

// Builder returns a ServiceBuilder
func (b *ServerBuilder) Build() *Server {
	return NewServer(b.Config)
}