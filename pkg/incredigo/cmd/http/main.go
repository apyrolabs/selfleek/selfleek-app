package main

import (
	"apyrolabs.com/sefleek/pkg/incredigo"
)

func main() {

	server := incredigo.Builder().Build()
	defer server.Close()
	httpServer:= server.HTTPServer()

	httpServer.Serve()
}
