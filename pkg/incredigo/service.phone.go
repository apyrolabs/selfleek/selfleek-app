package incredigo

import "log"

type mockPhoneService struct {
	infoLog *log.Logger
}

//NewMockPhoneService returns a new phone service that prints messages
//to the debug console
func NewMockPhoneService(infoLog *log.Logger) *mockPhoneService {
	return &mockPhoneService{
		infoLog: infoLog,
	}
}

func (s *mockPhoneService) SendMessage(phone, message string) error {
	s.infoLog.Printf(`Sending text message: {
		Phone: %v,
		Message: %v
	}`, phone, message)

	return nil
}
