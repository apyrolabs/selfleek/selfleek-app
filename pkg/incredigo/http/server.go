package http

import (
	"log"
	"net/http"

	"apyrolabs.com/sefleek/pkg/incredigo/auth"
)


type Config struct {
	Host string
}

//HttpServer
type Server struct {
	config *Config
	infoLog *log.Logger
	errorLog *log.Logger
	authService *auth.Service
}

func NewServer(config *Config, infoLog, errorLog *log.Logger,authService *auth.Service) *Server{
	return &Server{
		config:      config,
		infoLog:     infoLog,
		errorLog:    errorLog,
		authService: authService,
	}
}

func (s *Server) Serve() error {


	ctrlr := Controller{
		authService: s.authService,
		errorLog:    s.errorLog,
		infoLog:     s.infoLog,
	}

	server := http.Server{
		Addr:     s.config.Host,
		ErrorLog: s.errorLog,
		Handler:  ctrlr.routes(),
	}

	s.infoLog.Printf("Serving HTTP requests at: %s\n", server.Addr)
	err := server.ListenAndServe()
	s.errorLog.Fatal(err)
	return err
}
