package http

import (
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	errs "github.com/pkg/errors"
	"log"
	"net/http"

	"apyrolabs.com/sefleek/pkg/incredigo/auth"
)

type Controller struct {
	authService *auth.Service
	errorLog    *log.Logger
	infoLog     *log.Logger
}

func (c *Controller) routes() *http.ServeMux {
	// Use the http.NewServeMux() function to initialize a new servemux
	mux := http.NewServeMux()
	mux.HandleFunc("/auth/register", c.RegisterUser)
	mux.HandleFunc("/auth/login/user", c.AuthByUser)
	mux.HandleFunc("/auth/login/email", c.AuthByEmail)
	mux.HandleFunc("/auth/login/phone", c.AuthByPhone)
	mux.HandleFunc("/auth/phone/request-verification", c.RequestPhoneVerification)
	mux.HandleFunc("/auth/phone/verify", c.VerifyPhone)
	mux.HandleFunc("/auth/email/request-verification", c.RequestEmailVerification)
	mux.HandleFunc("/auth/email/verify", c.VerifyEmail)
	return mux
}

//RegisterUser creates a new user
func (c *Controller) RegisterUser(w http.ResponseWriter, r *http.Request) {
	newUser := &auth.RegistrationInput{}

	err := json.NewDecoder(r.Body).Decode(newUser)
	if err != nil {
		c.serverError(w, err)
		return
	}
	_, err = c.authService.Register(newUser)
	if err != nil {
		c.serverError(w, err)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
}

//AuthByUser authenticates by username
func (c *Controller) AuthByUser(w http.ResponseWriter, r *http.Request) {
	login := &auth.UserAuthInput{}

	err := json.NewDecoder(r.Body).Decode(login)
	if err != nil {
		c.serverError(w, err)
		return
	}
	authorized, err := c.authService.AuthByUser(login)
	if err != nil {
		c.errorLog.Println(err)
	}
	if !authorized {
		c.clientError(w, http.StatusUnauthorized)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
}

//AuthByEmail authenticates by Email
func (c *Controller) AuthByEmail(w http.ResponseWriter, r *http.Request) {
	login := &auth.EmailAuthInput{}

	err := json.NewDecoder(r.Body).Decode(login)
	if err != nil {
		c.serverError(w, err)
		return
	}
	authorized, err := c.authService.AuthByEmail(login)
	if err != nil {
		c.errorLog.Println(err)
	}
	if !authorized {
		c.clientError(w, http.StatusUnauthorized)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
}

//AuthByPhone authenticates by Phone
func (c *Controller) AuthByPhone(w http.ResponseWriter, r *http.Request) {
	login := &auth.PhoneAuthInput{}

	err := json.NewDecoder(r.Body).Decode(login)
	if err != nil {
		c.serverError(w, err)
		return
	}
	authorized, err := c.authService.AuthByPhone(login)
	if err != nil {
		c.errorLog.Println(err)
	}
	if !authorized {
		c.clientError(w, http.StatusUnauthorized)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
}

//RequestPhoneVerification
func (c *Controller) RequestPhoneVerification(w http.ResponseWriter, r *http.Request) {
	input := &RequestPhoneVerificationInput{}
	err := json.NewDecoder(r.Body).Decode(input)
	if err != nil {
		c.serverError(w, err)
		return
	}

	if input.AccountID == nil {
		c.serverError(w, errors.New("input.AccountID is a required field"))
		return
	}

	err = c.authService.RequestPhoneVerification(*input.AccountID)
	if err != nil {
		c.serverError(w, err)
		return
	}
}

//RequestEmailVerification
func (c *Controller) RequestEmailVerification(w http.ResponseWriter, r *http.Request) {
	input := &RequestPhoneVerificationInput{}
	err := json.NewDecoder(r.Body).Decode(input)
	if err != nil {
		c.serverError(w, err)
		return
	}

	if input.AccountID == nil {
		c.serverError(w, errors.New("input.AccountID is a required field"))
		return
	}

	err = c.authService.RequestEmailVerification(*input.AccountID)
	if err != nil {
		c.serverError(w, err)
		return
	}
}

//VerifyEmail
func (c *Controller) VerifyEmail(w http.ResponseWriter, r *http.Request){
	input:= &struct {
		AccountID uuid.UUID
		Key string
	}{}

	err:= json.NewDecoder(r.Body).Decode(input)
	if err!=nil {
		c.serverError(w, errs.Wrap(err, "error parsing input"))
		return
	}

	err = c.authService.VerifyEmail(input.AccountID, input.Key)
	if err != nil {
		c.serverError(w, errs.Wrap(err, "could not verify email"))
		return
	}
}

//VerifyPhone
func (c *Controller) VerifyPhone(w http.ResponseWriter, r *http.Request){
	input:= &struct {
		AccountID uuid.UUID
		Key string
	}{}

	err:= json.NewDecoder(r.Body).Decode(input)
	if err!=nil {
		c.serverError(w, errs.Wrap(err, "error parsing input"))
		return
	}

	err = c.authService.VerifyPhone(input.AccountID, input.Key)
	if err != nil {
		c.serverError(w, errs.Wrap(err, "could not verify phone"))
		return
	}
}
