package http

import (
	"fmt"
	"net/http"
	"runtime/debug"
)

// The serverERror helper writes an error message and stack trace to the errorLog
/// then sends a generic 500 internal Server Error response to the user
func (c *Controller) serverError(w http.ResponseWriter, err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	c.errorLog.Output(2, trace)

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

// The clientError helper sends a specific status code and corresponding descirption
// to the user when there is a problem with their request.
func (c *Controller) clientError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
}

// This is a convenience wrapper around clientError which sends a 404 response to the user
func (c *Controller) notFound(w http.ResponseWriter) {
	c.clientError(w, http.StatusNotFound)
}
