package http

import "github.com/google/uuid"

type RequestPhoneVerificationInput struct {
	AccountID *uuid.UUID
}

type RequestEmailVerificationInput struct {
	AccountID *uuid.UUID
}
