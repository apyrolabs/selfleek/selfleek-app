module apyrolabs.com/sefleek

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/lib/pq v1.3.0
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20200219234226-1ad67e1f0ef4
)
