/**
* This package starts an http server that
* hosts Selfleek App
 */
package main

import (
	"fmt"
	"sync"

	"apyrolabs.com/sefleek/pkg/docstore"
	"apyrolabs.com/sefleek/pkg/selfleek/app"
	"apyrolabs.com/sefleek/pkg/selfleek/app/http"
)

func main() {

	cfg := &http.ServerConfig{
		Host: ":4400",
		App: app.AppConfig{
			AppDsn:      "user=postgres password=password host=localhost port=5432 dbname=selfleek_app sslmode=disable",
			DocumentDsn: "user=postgres password=password host=localhost port=5432 dbname=selfleek_documents sslmode=disable",
		},
	}

	docCfg := &docstore.Config{
		Dsn:       "user=postgres password=password host=localhost port=5432 dbname=selfleek_documents sslmode=disable",
		Host:      ":4500",
		ServePath: "/",
		StaticDir: "",
	}
	fmt.Println("Main: Starting Up...")
	var wg sync.WaitGroup
	wg.Add(1)
	go http.StartServer(cfg)
	wg.Add(1)
	go docstore.StartServer(docCfg)
	wg.Wait()
	fmt.Println("Main: Completed")
}
